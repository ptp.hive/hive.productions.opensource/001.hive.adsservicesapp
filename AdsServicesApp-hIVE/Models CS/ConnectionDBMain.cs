﻿using System;
//
using System.Windows.Forms;
using System.Data.SQLite;

namespace AdsServicesApp_hIVE.Models_CS
{
    class ConnectionDBMain : IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region ConnectionDataBase
        //----------

        private static string DSPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                       "\\" + Application.CompanyName +
                                       "\\" + Application.ProductName + " " +
                                              Application.ProductVersion.Substring(0, 1);

        private static string DbName = "adsedb.asdb";

        private static string DbPass = Application.ProductName +
                                       " ver" + Application.ProductVersion.Substring(0, 1) +
                                       " 28749-55676-98877-56585;";

        // For Test ...
        private static string DSStartupPath = Application.StartupPath + "\\DataBase";
        private static string DbNoPassword = "";

        private static string MainStrConnection = ("Data Source=" + DSStartupPath +
                                                   "\\" + DbName + "; " +
                                                   "Version=3; " +
                                                   "Password=" + DbNoPassword);

        private static SQLiteConnection MainConnection = new SQLiteConnection(MainStrConnection);

        //----------
        #endregion ConnectionDataBase

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        public string GetStrCon
        {
            get { return MainStrConnection; }
        }

        public SQLiteConnection GetCon
        {
            get { return MainConnection; }
        }
    }
}

