﻿using System;
//
using System.Windows.Forms;
using System.Drawing.Text;
using System.Drawing;
//
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.IO;

namespace AdsServicesApp_hIVE.Models_CS
{
    class Prerequisites : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool RunPrerequisites()
        {
            try
            {
                FontOperation.SetupFontsPath();
                DatabaseOperation.SetMainStrConnection(false);
            }
            catch { return false; }

            return true;
        }
    }

    public class FontOperation
    {
        [DllImport("gdi32")]
        private static extern int AddFontResource(string strFontFilePath);

        [DllImport("user32.dll")]
        private static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
        const int HWND_BROADCAST = 0xFFFF;
        const int WM_FONTCHANGE = 0x1D;

        public static bool FontIsAvailable(string strFontName)
        {
            InstalledFontCollection AllFonts = new InstalledFontCollection();
            foreach (FontFamily ff in AllFonts.Families)
            {
                if (ff.Name == strFontName) { return true; }
            }

            return false;
        }

        public static bool InstallFontInWindows(string strFontFilePath, bool ShowError)
        {
            string strFileName = Path.GetFileName(strFontFilePath);

            try
            {
                //Step 1 : Copy font file to Fonts directory
                string strFontsPathFile = Environment.GetEnvironmentVariable("windir") + @"\Fonts\" + strFileName;
                File.Copy(strFontFilePath, strFontsPathFile, true);

                //Step 2 : Install font in resource of MS-Windows
                if (AddFontResource(strFontsPathFile) == 0) { return false; }

                //Step 3 : Set registry information of new installed font for use
                RegistryKey reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts", true);
                reg.SetValue(strFileName.Split('.')[0] + " (TrueType)", strFileName);
                reg.Close();

                SendMessage(HWND_BROADCAST, WM_FONTCHANGE, 0, 0);
            }
            catch (Exception exc)
            {
                if (ShowError)
                {
                    MessageBox.Show("خطا در ثبت فونت " + strFileName + " \n" + exc.Message);
                }
                return false;
            }

            return true;
        }

        public static bool SetupFontsPath()
        {
            string FontsPath = System.Windows.Forms.Application.StartupPath + "\\Fonts";

            try
            {
                string[] FileFonts;

                FileFonts = System.IO.Directory.GetFiles(FontsPath);

                foreach (string xFonts in FileFonts)
                {
                    try
                    {
                        InstallFontInWindows(xFonts, false);
                    }
                    catch { return false; }
                }
            }
            catch { return false; }

            return true;
        }
    }

    public class DatabaseOperation
    {
        private static ConnectionDBMain _ConDB = new ConnectionDBMain();

        public static bool SetMainStrConnection(bool ShowError)
        {
            try
            {
                SettingsDB.Default.MainStrConnection = _ConDB.GetStrCon;
                return true;
            }
            catch (Exception exc)
            {
                if (ShowError)
                {
                    MessageBox.Show("خطا در ثبت کانکشن اصلی پایگاه‌داده. " + " \n" + exc.Message);
                }
                return false;
            }
            return false;
        }
    }
}
