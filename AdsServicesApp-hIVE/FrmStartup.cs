﻿using System;
using System.Drawing;
using System.Windows.Forms;
//
using AdsServicesApp_hIVE.Models_CS;
using ECP.PersianMessageBox;
using System.Diagnostics;
using System.Data.SQLite;
using System.IO;

namespace AdsServicesApp_hIVE
{
    public partial class FrmStartup : Form
    {
        public FrmStartup()
        {
            InitializeComponent();
        }

        #region ConnectionModels
        //----------

        private SQLiteConnection ConDB = new SQLiteConnection(SettingsDB.Default.MainStrConnection);

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionModels

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Form Main
        //----------

        #region FormDragwithMouse

        private void frmDragForm_Paint(object sender, PaintEventArgs e)
        {
            //Draws a border to make the Form stand out
            //Just done for appearance, not necessary

            Pen p = new Pen(Color.Gray, 3);
            e.Graphics.DrawRectangle(p, 0, 0, this.Width - 1, this.Height - 1);
            p.Dispose();
        }

        Point lastClick; //Holds where the Form was clicked

        private void frmDragForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void frmDragForm_MouseMove(object sender, MouseEventArgs e)
        {
            //Point newLocation = new Point(e.X - lastE.X, e.Y - lastE.Y);
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        //If the user clicks on the Objects(on the form) we want the same kind of behavior
        //so we just call the Forms corresponding methods
        private void frmDragObjects_MouseDown(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseDown(sender, e);
        }

        private void frmDragObjects_MouseMove(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseMove(sender, e);
        }

        #endregion FormDragwithMouse

        ////----------

        #region toolStripMenu

        private void StartupForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید خارج شوید؟   \n",
                                       "خروج",
                                       PersianMessageBox.Buttons.YesNo,
                                       PersianMessageBox.Icon.Question,
                                       PersianMessageBox.DefaultButton.Button2) == DialogResult.Yes)
            {
                System.Globalization.CultureInfo languageEn = new System.Globalization.CultureInfo("en-US");
                InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(languageEn);
            }
            else { e.Cancel = true; }
        }

        #endregion toolStripMenu

        ////----------

        #region toolStripLogo

        private void toolStripLogo_MouseHover(object sender, EventArgs e)
        {
            if (this.WindowState != System.Windows.Forms.FormWindowState.Maximized)
            {
                this.toolStripLogo.Cursor = System.Windows.Forms.Cursors.SizeAll;
            }
            else
            {
                this.toolStripLogo.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion toolStripLogo

        ////----------

        #region Others Form Main

        private void tabControlMain_Enter(object sender, EventArgs e)
        {
            tabControlMain.ItemSize = new System.Drawing.Size((tabControlMain.Width / tabControlMain.TabCount) - 11, 40);
        }

        private void FrmStartup_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                tabControlMain.ItemSize = new System.Drawing.Size((tabControlMain.Width / tabControlMain.TabCount) - 11, 40);
            }
            catch { }
        }

        #endregion toolStripLogo

        //----------
        #endregion Form Main

        #region Page Main
        //----------

        private void LoadPageMain()
        {
            try
            {
                LoadTableImages();
                RefreshListAds();

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch
            {
                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
        }

        private void LoadTableImages()
        {
            imgListTableAds256x144.Images.Clear();
            imgListTableAds256x144.ImageSize = new System.Drawing.Size(256, 144);

            int KeyName = -1;

            string Q_ImgLoad = "SELECT * FROM Images" +
                               " ORDER BY Im_ID ASC;";

            SQLiteCommand Imgcom = new SQLiteCommand(Q_ImgLoad, ConDB);

            ConDB.Open();
            try
            {
                SQLiteDataReader Imgdr = Imgcom.ExecuteReader();

                while (Imgdr.Read())
                {
                    byte[] imageBytes = (byte[])Imgdr.GetValue(2);
                    MemoryStream ms = new MemoryStream(imageBytes);
                    Bitmap bmap = new Bitmap(ms);

                    KeyName++;

                    imgListTableAds256x144.Images.Add(bmap);
                    imgListTableAds256x144.Images.SetKeyName(KeyName, Imgdr["Im_ID"].ToString());
                }
                Imgdr.Close();
            }
            catch (Exception exp)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۱۱۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۱۱۰: " + "خطا در بارگیری تصاویر تبلیغاتی.   ";
                }
            }
            ConDB.Close();
        }

        private void RefreshListAds()
        {
            listViewAdsShow.Items.Clear();

            string Q_AdsSelect = "SELECT * FROM Advertise" +
                                 " WHERE Ads_Visible=1" +
                                 " ORDER BY Ads_ID ASC;";

            SQLiteCommand Adscom = new SQLiteCommand(Q_AdsSelect, ConDB);

            ConDB.Open();
            try
            {
                SQLiteDataReader Adsdr = Adscom.ExecuteReader();
                while (Adsdr.Read())
                {
                    listViewAdsShow.Items.Add(Adsdr["Ads_ID"].ToString(), Adsdr["Ads_Title"].ToString(), Int32.Parse(Adsdr["Ads_ImageID"].ToString()) - 1);
                }
                Adsdr.Close();
            }
            catch (Exception exp)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۲۱۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۲۱۰: " + "خطا در بارگیری اطلاعات تبلیغ.   ";
                }
            }
            ConDB.Close();
        }

        private void LoadDetailsAdsID(int AdsID)
        {
            string LinkPage = string.Empty;

            string Q_AdsSelect = "SELECT * FROM Advertise" +
                                 " WHERE Ads_ID=" + AdsID +
                                 " ORDER BY Ads_ID;";

            SQLiteCommand Adscom = new SQLiteCommand(Q_AdsSelect, ConDB);

            ConDB.Open();
            try
            {
                SQLiteDataReader Adsdr = Adscom.ExecuteReader();
                if (Adsdr.Read())
                {
                    LinkPage = Adsdr["Ads_LinkPage"].ToString();
                    ProcessStartApp(LinkPage, "");
                }
                Adsdr.Close();
            }
            catch (Exception exp)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۳۱۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_tabPageMain + "۰۳۱۰: " + "خطا در بارگیری اطلاعات تبلیغ.   ";
                }
            }
            ConDB.Close();
        }

        private void listViewAdsShow_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listViewAdsShow.SelectedIndices.Count <= 0)
                {
                    return;
                }
                int selectedindex = listViewAdsShow.SelectedIndices[0];
                if (selectedindex >= 0)
                {
                    SettingsDB.Default.NowAdsID = Int32.Parse(listViewAdsShow.Items[selectedindex].Name);
                    LoadDetailsAdsID(SettingsDB.Default.NowAdsID);

                    _TryCatch.GetCEM_Error(MesExpErrorForms);
                    MesExpErrorForms = "";
                }

                listViewAdsShow.FocusedItem.Selected = false;
                return;
            }
            catch { }
        }

        //----------
        #endregion Page Main

        #region Page Tools
        //----------

        private void LoadPageTools()
        {
            try
            {
                txtCnfHotspotNameWebsite.Text = "_hIVE72.iR";
                txtCnfHotspotNameWebsite.Text.Replace(" ", "");
                txtCnfHotspotNameWebsite.ReadOnly = true;

                LoadVisualConnectify();
                isRunedConnectify();

                rtbCMDCommands.Text = "";
                txtCnfHotspotPassword.UseSystemPasswordChar = true;
                txtCnfPassword_UseSysPassChar();

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch { }
        }

        private bool ShowHostedNetwork()
        {
            try
            {
                string _cmdArguments = string.Empty;

                _cmdArguments = "Netsh WLAN Show HostedNetwork";
                ExecuteCMD(_cmdArguments);
            }
            catch { }

            return false;
        }

        private void isRunedConnectify()
        {
            try
            {
                //ShowHostedNetwork();
                bool isRuned = SettingsTools.Default.isRunedHotspot;
                //
                txtCnfHotspotName.Enabled = !isRuned;
                txtCnfHotspotPassword.Enabled = !isRuned;

                if (isRuned)
                {
                    SettingsTools.Default.HotspotName = txtCnfHotspotName.Text;
                    SettingsTools.Default.HotspotPassword = txtCnfHotspotPassword.Text;
                    SettingsTools.Default.Save();

                    btnStartStopConnectify.Text = "Stop Hotspot";
                }
                else
                {
                    btnStartStopConnectify.Text = "Start Hotspot";
                }
            }
            catch { }
        }

        private void LoadVisualConnectify()
        {
            try
            {
                if (SettingsTools.Default.HotspotName == "")
                {
                    txtCnfHotspotName.Text = Application.CompanyName;
                }
                else
                {
                    txtCnfHotspotName.Text = SettingsTools.Default.HotspotName;
                }

                txtCnfHotspotPassword.Text = SettingsTools.Default.HotspotPassword;
            }
            catch { }
        }

        private void AddCMDCommands(string strCommand)
        {
            try
            {
                if (strCommand != "")
                {
                    string _DateTime = DateTime.Now.Year.ToString() + "/" +
                                       DateTime.Now.Month.ToString() + "/" +
                                       DateTime.Now.Day.ToString() + "  " +
                                       DateTime.Now.Hour.ToString() + ":" +
                                       DateTime.Now.Minute.ToString() + ":" +
                                       DateTime.Now.Second.ToString();

                    strCommand = strCommand.TrimStart('\r', '\n');
                    strCommand = strCommand.TrimEnd('\r', '\n');

                    if (rtbCMDCommands.Text != "")
                    {
                        rtbCMDCommands.Text += "\n";
                    }
                    rtbCMDCommands.Text += _DateTime + " =>  " +
                                           strCommand.Replace("\r\n\r\n", "\n\n" + _DateTime + " =>  ").
                                           Replace("\r\n", "\r\n" + _DateTime + " =>  ") + "\n";

                    rtbCMDCommands.SelectionStart = rtbCMDCommands.Text.Length;
                    rtbCMDCommands.ScrollToCaret();
                }
            }
            catch { }
        }

        private bool ProcessStartApp(string exeNameApp, string ShowNameApp)
        {
            try
            {
                Process.Start(exeNameApp);
                if (ShowNameApp != "")
                {
                    AddCMDCommands("Started " + ShowNameApp + ".");
                }
            }
            catch { return false; }

            return true;
        }

        private bool ReportExecuteCMDCommand(Process _Process)
        {
            bool noError = true;

            try
            {
                string _errorMessage = string.Empty;
                string _outputMessage = string.Empty;

                // Instructs the Process component to wait indefinitely for the associated process to exit.
                _errorMessage = _Process.StandardError.ReadToEnd().ToString();
                _Process.WaitForExit();
                if (_errorMessage != "") { noError = false; }
                AddCMDCommands(_errorMessage);

                // Instructs the Process component to wait indefinitely for the associated process to exit.
                _outputMessage = _Process.StandardOutput.ReadToEnd().ToString();
                _Process.WaitForExit();

                if (_outputMessage == "The hosted network started. \r\n\r\n")
                {
                    SettingsTools.Default.isRunedHotspot = true;
                    SettingsTools.Default.Save();
                }
                else if (_outputMessage == "The hosted network stopped. \r\n\r\n")
                {
                    SettingsTools.Default.isRunedHotspot = false;
                    SettingsTools.Default.Save();
                }

                AddCMDCommands(_outputMessage);
            }
            catch { return false; }

            return noError;
        }

        private bool ExecuteCMD(string _cmdArguments)
        {
            try
            {
                System.Diagnostics.Process _Process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo _ProcessStartInfo = new System.Diagnostics.ProcessStartInfo();
                //
                _ProcessStartInfo.WorkingDirectory = @"C:\Windows\System32";
                _ProcessStartInfo.FileName = @"C:\Windows\System32\cmd.exe";
                //_ProcessStartInfo.FileName = "PowerShell.exe";
                _ProcessStartInfo.Arguments = "/C " + _cmdArguments;
                //
                _ProcessStartInfo.Verb = "RunAs";
                _ProcessStartInfo.UseShellExecute = false;
                _ProcessStartInfo.RedirectStandardError = true;
                _ProcessStartInfo.RedirectStandardOutput = true;
                _ProcessStartInfo.CreateNoWindow = true;
                _ProcessStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //
                _Process.StartInfo = _ProcessStartInfo;
                _Process.Start();
                _Process.WaitForExit();

                return ReportExecuteCMDCommand(_Process);
            }
            catch (Exception Exp) { return false; }
        }

        //----------

        private void btnNotepad_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartApp("notepad.exe", "Notepad");
            }
            catch { }
        }

        private void btnCalculator_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartApp("calc.exe", "Calculator");
            }
            catch { }
        }

        private void btnPaint_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessStartApp("mspaint.exe", "MSPaint");
            }
            catch { }
        }

        private void txtCnfHotspotName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtCnfHotspotName.Text = txtCnfHotspotName.Text.Replace(" ", "");
            }
            catch { }
        }

        private void txtCnfHotspotPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 32)
                {

                }
            }
            catch { }
        }

        private void txtCnfHotspotPassword_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtCnfHotspotPassword.Text = txtCnfHotspotPassword.Text.Replace(" ", "");
            }
            catch { }
        }

        private bool txtCnfPassword_UseSysPassChar()
        {
            bool noError = true;

            try
            {
                if (txtCnfHotspotPassword.UseSystemPasswordChar)
                {
                    btnShowHidePassword.Text = "نمایش";
                }
                else
                {
                    btnShowHidePassword.Text = "مخفی";
                }
            }
            catch { noError = false; }

            return noError;
        }

        private void btnShowHidePassword_Click(object sender, EventArgs e)
        {
            try
            {
                txtCnfHotspotPassword.UseSystemPasswordChar = !txtCnfHotspotPassword.UseSystemPasswordChar;
                txtCnfPassword_UseSysPassChar();
            }
            catch { }
        }

        private void btnClearCMDCommands_Click(object sender, EventArgs e)
        {
            try
            {
                if (PersianMessageBox.Show("آیا می‌خواهید گزارشات خط فرمان پاک شوند؟   ",
                                           "ابزارها",
                                           PersianMessageBox.Buttons.YesNo,
                                           PersianMessageBox.Icon.Question,
                                           PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                {
                    rtbCMDCommands.Text = "";
                }
            }
            catch { }
        }

        private bool isErrorSSConnectify()
        {
            bool isError = false;
            SettingsTools.Default.MessageSSConnectify = "قسمت‌های زیر را رعایت کنید:      \n";

            try
            {
                int MaxCountChHSName = 32;
                MaxCountChHSName -= txtCnfHotspotNameWebsite.TextLength;

                if (txtCnfHotspotName.Text == null || txtCnfHotspotName.Text == "" || txtCnfHotspotName.TextLength > MaxCountChHSName)
                {
                    SettingsTools.Default.MessageSSConnectify += "\n نام شبکه « حداکثر " + MaxCountChHSName + " کاراکتری » وارد نمایید. ";
                    isError = true;
                }

                if (txtCnfHotspotPassword.Text == null || txtCnfHotspotPassword.Text == "" || txtCnfHotspotPassword.Text.Length < 8)
                {
                    SettingsTools.Default.MessageSSConnectify += "\n کلمه عبور « حداقل ۸ کاراکتری » وارد نمایید. ";
                    isError = true;
                }
            }
            catch { isError = true; }

            return isError;
        }

        private void btnStartStopConnectify_Click(object sender, EventArgs e)
        {
            try
            {
                string _cmdArguments = string.Empty;

                if (!SettingsTools.Default.isRunedHotspot)
                {
                    if (!isErrorSSConnectify())
                    {
                        btnStartStopConnectify.Text = "Hotspot the Starting ...";

                        _cmdArguments = "Netsh WLAN Set HostedNetwork" +
                                       " Mode=Allow" +
                                       " SSID=" + txtCnfHotspotName.Text.Replace(" ", "") + txtCnfHotspotNameWebsite.Text.Replace(" ", "") +
                                       " Key=" + txtCnfHotspotPassword.Text.Replace(" ", "");

                        if (ExecuteCMD(_cmdArguments))
                        {
                            _cmdArguments = "Netsh WLAN Start HostedNetwork";
                            ExecuteCMD(_cmdArguments);
                        }

                        isRunedConnectify();

                    }
                    else
                    {
                        PersianMessageBox.Show(SettingsTools.Default.MessageSSConnectify,
                                               "خطا",
                                               PersianMessageBox.Buttons.OK,
                                               PersianMessageBox.Icon.Error,
                                               PersianMessageBox.DefaultButton.Button1);
                    }
                }
                else
                {
                    btnStartStopConnectify.Text = "Hotspot the Stoping ...";

                    _cmdArguments = "Netsh WLAN Stop HostedNetwork";
                    ExecuteCMD(_cmdArguments);

                    isRunedConnectify();
                }

            }
            catch (Exception Exp)
            {
                isRunedConnectify();
            }
        }

        //----------
        #endregion Page Tools

        #region Page Settings
        //----------

        private void LoadPageSettings()
        {
            try
            {
                #region grbStartup

                chbStartupApp.Checked = SettingsMain.Default.StartupApp;
                chbStartupServices.Checked = SettingsMain.Default.StartupServices;
                chbStartupNotifications.Checked = SettingsMain.Default.StartupNotifications;

                #endregion grbStartup

                #region grbDisplay

                chbShowNotificationPopups.Checked = SettingsMain.Default.ShowNotificationPopups;
                chbAlwaysShowAccessIcons.Checked = SettingsMain.Default.AlwaysShowAccessIcons;
                chbShowAllServices.Checked = SettingsMain.Default.ShowAllServices;
                chbShowNewsBanners.Checked = SettingsMain.Default.ShowNewsBanners;
                chbStartupTopMost.Checked = SettingsMain.Default.StartupTopMost;

                #endregion grbDisplay

                #region grbAdvanced

                chbChechForUpdates.Checked = SettingsMain.Default.ChechForUpdates;
                chbAutomaticUpdateCheck.Checked = SettingsMain.Default.AutomaticUpdateCheck;
                chbAutomaticUpdate.Checked = SettingsMain.Default.AutomaticUpdate;
                cmbSelectedLanguage.SelectedIndex = SettingsMain.Default.SelectedLanguage;

                #endregion grbAdvanced

                #region tlpSaveSettings

                ChangeSettings(false);

                #endregion tlpSaveSettings

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch { }
        }

        #region ChangeSettings

        private bool isChangedSettings()
        {
            #region grbStartup

            if (SettingsMain.Default.StartupApp !=
                Convert.ToBoolean(SettingsMain.Default.PropertyValues["StartupApp"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.StartupServices !=
                Convert.ToBoolean(SettingsMain.Default.PropertyValues["StartupServices"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.StartupNotifications !=
                Convert.ToBoolean(SettingsMain.Default.PropertyValues["StartupNotifications"].Property.DefaultValue))
            { return true; }

            #endregion grbStartup

            #region grbDisplay

            if (SettingsMain.Default.ShowNotificationPopups !=
                Convert.ToBoolean(SettingsMain.Default.PropertyValues["ShowNotificationPopups"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.AlwaysShowAccessIcons !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["AlwaysShowAccessIcons"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.ShowAllServices !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["ShowAllServices"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.ShowNewsBanners !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["ShowNewsBanners"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.StartupTopMost !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["StartupTopMost"].Property.DefaultValue))
            { return true; }

            #endregion grbDisplay

            #region grbAdvanced

            if (SettingsMain.Default.ChechForUpdates !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["ChechForUpdates"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.AutomaticUpdateCheck !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["AutomaticUpdateCheck"].Property.DefaultValue))
            { return true; }

            if (SettingsMain.Default.AutomaticUpdate !=
                            Convert.ToBoolean(SettingsMain.Default.PropertyValues["AutomaticUpdate"].Property.DefaultValue))
            { return true; }

            #endregion grbAdvanced

            #region tlpSaveSettings

            if (SettingsMain.Default.SelectedLanguage !=
                            Convert.ToInt32(SettingsMain.Default.PropertyValues["SelectedLanguage"].Property.DefaultValue))
            { return true; }

            #endregion tlpSaveSettings

            return false;
        }

        private void ChangeSettings(bool isChanged)
        {
            try
            {
                btnOKSettings.Visible = isChanged;
                btnCancleSettings.Visible = isChanged;
                //
                btnDefaultSettings.Visible = isChangedSettings();
            }
            catch { }
        }

        private void chbStartupApp_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbStartupServices_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbStartupNotifications_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbShowNotificationPopups_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbAlwaysShowAccessIcons_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbShowAllServices_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbShowNewsBanners_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbStartupTopMost_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbChechForUpdates_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbAutomaticUpdateCheck_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void chbAutomaticUpdate_CheckedChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        private void cmbSelectedLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeSettings(true);
        }

        #endregion ChangeSettings

        private void btnOKSettings_Click(object sender, EventArgs e)
        {
            try
            {
                if (PersianMessageBox.Show("آیا می‌خواهید تنظیمات جدید ثبت شوند؟   ",
                                           "ثبت تغییرات",
                                           PersianMessageBox.Buttons.YesNo,
                                           PersianMessageBox.Icon.Question,
                                           PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                {
                    SettingsMain.Default.StartupApp = chbStartupApp.Checked;
                    SettingsMain.Default.StartupServices = chbStartupServices.Checked;
                    SettingsMain.Default.StartupNotifications = chbStartupNotifications.Checked;
                    //
                    SettingsMain.Default.ShowNotificationPopups = chbShowNotificationPopups.Checked;
                    SettingsMain.Default.AlwaysShowAccessIcons = chbAlwaysShowAccessIcons.Checked;
                    SettingsMain.Default.ShowAllServices = chbShowAllServices.Checked;
                    SettingsMain.Default.ShowNewsBanners = chbShowNewsBanners.Checked;
                    SettingsMain.Default.StartupTopMost = chbStartupTopMost.Checked;
                    //
                    SettingsMain.Default.ChechForUpdates = chbChechForUpdates.Checked;
                    SettingsMain.Default.AutomaticUpdateCheck = chbAutomaticUpdateCheck.Checked;
                    SettingsMain.Default.AutomaticUpdate = chbAutomaticUpdate.Checked;
                    SettingsMain.Default.SelectedLanguage = cmbSelectedLanguage.SelectedIndex;
                    //
                    SettingsMain.Default.Save();

                    ChangeSettings(false);
                }
            }
            catch { }
        }

        private void btnCancleSettings_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPageSettings();
            }
            catch { }
        }

        private void btnDefaultSettings_Click(object sender, EventArgs e)
        {
            try
            {
                if (PersianMessageBox.Show("آیا می‌خواهید تنظیمات به حالت پیش‌فرض بازگردد؟   ",
                                           "تنظیمات پیش‌فرض",
                                           PersianMessageBox.Buttons.YesNo,
                                           PersianMessageBox.Icon.Question,
                                           PersianMessageBox.DefaultButton.Button1) == DialogResult.Yes)
                {
                    SettingsMain.Default.Reset();
                    SettingsTools.Default.Reset();
                    LoadPageSettings();
                    LoadPageTools();
                }
            }
            catch { }
        }

        //----------
        #endregion Page Settings

        #region Page About US
        //----------

        private void LoadPageAboutUS()
        {
            try
            {
                lblVersion.Text = "نسخه: " + Application.ProductVersion.Substring(0, 4);

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch { }
        }

        //----------
        #endregion Page About US

        //------------
        ////----------////---------------------------------------------------------------------// Begin FrmStartup_Load
        //------------

        private void FrmStartup_Load(object sender, EventArgs e)
        {
            string MessageError = string.Empty;

            try
            {
                LoadPageMain();
                LoadPageTools();
                LoadPageSettings();
                LoadPageAboutUS();

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch
            {
                MessageError = "\nفرآیند بارگیری برنامه با خطا همراه بود.   \n" +
                               "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                   "خطا",
                                   PersianMessageBox.Buttons.OK,
                                   PersianMessageBox.Icon.Error,
                                   PersianMessageBox.DefaultButton.Button1);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End FrmStartup_Load
        //------------
    }
}