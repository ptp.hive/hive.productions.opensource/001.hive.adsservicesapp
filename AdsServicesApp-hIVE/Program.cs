﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
//
using ECP.PersianMessageBox;
using AdsServicesApp_hIVE.Models_CS;

namespace AdsServicesApp_hIVE
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                
                try
                {
                    Prerequisites _Prerequisites = new Prerequisites();
                    _Prerequisites.RunPrerequisites();
                }
                catch { }

                Application.Run(new FrmStartup());
            }
            catch (Exception)
            {
                string MessageError = "\nاجرا و نمایش صحیح برنامه‌ی حسابدار دانشجو، نیازمند موارد زیر می‌باشد:   " +
                                      "\n۱- فونت‌های B Nazanin و B Jadid" +
                                      "\n۲- Microsoft .NET Framework 4" +
                                      "\n۳- Microsoft Visual C++ 2010 Redistributable x86" +
                                      "\nلطفاً به وجود پیش‌نیازهای ذکر شده در ویندوز خود، بعد از فرآیند نصب توجه نمایید." +
                                      "\nدر صورت ناخوانا بودن نوشته‌ها، لطفاً مجموعه‌ی فونت‌های" +
                                        " همراه این برنامه را مجدداً نصب کنید." +
                                      "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                       "خطا",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Error,
                                       PersianMessageBox.DefaultButton.Button1);
            }
        }
    }
}
