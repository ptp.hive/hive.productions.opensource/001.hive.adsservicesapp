﻿namespace AdsServicesApp_hIVE
{
    partial class FrmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStartup));
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblMainProductName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnSettings = new System.Windows.Forms.ToolStripButton();
            this.btnTools = new System.Windows.Forms.ToolStripButton();
            this.btnAboutUS = new System.Windows.Forms.ToolStripButton();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageMain = new System.Windows.Forms.TabPage();
            this.listViewAdsShow = new System.Windows.Forms.ListView();
            this.imgListTableAds256x144 = new System.Windows.Forms.ImageList(this.components);
            this.tabPageTools = new System.Windows.Forms.TabPage();
            this.tlpTools = new System.Windows.Forms.TableLayoutPanel();
            this.tlpUpTools = new System.Windows.Forms.TableLayoutPanel();
            this.btnNotepad = new System.Windows.Forms.Button();
            this.imageListButton56 = new System.Windows.Forms.ImageList(this.components);
            this.btnCalculator = new System.Windows.Forms.Button();
            this.btnPaint = new System.Windows.Forms.Button();
            this.splitContainerConnectify = new System.Windows.Forms.SplitContainer();
            this.tlpVisualConnectify = new System.Windows.Forms.TableLayoutPanel();
            this.tlpCnfHotspotName = new System.Windows.Forms.TableLayoutPanel();
            this.txtCnfHotspotNameWebsite = new System.Windows.Forms.TextBox();
            this.txtCnfHotspotName = new System.Windows.Forms.TextBox();
            this.lblCnfDetailsProcess = new System.Windows.Forms.Label();
            this.lblCnfHotspotName = new System.Windows.Forms.Label();
            this.lblCnfPassword = new System.Windows.Forms.Label();
            this.btnStartStopConnectify = new System.Windows.Forms.Button();
            this.tlpCnfHotspotPassword = new System.Windows.Forms.TableLayoutPanel();
            this.txtCnfHotspotPassword = new System.Windows.Forms.TextBox();
            this.btnShowHidePassword = new System.Windows.Forms.Button();
            this.panelCMDCommands = new System.Windows.Forms.Panel();
            this.btnClearCMDCommands = new System.Windows.Forms.Button();
            this.rtbCMDCommands = new System.Windows.Forms.RichTextBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.tlpSettings = new System.Windows.Forms.TableLayoutPanel();
            this.grbStartup = new System.Windows.Forms.GroupBox();
            this.flpStartup = new System.Windows.Forms.FlowLayoutPanel();
            this.chbStartupApp = new System.Windows.Forms.CheckBox();
            this.chbStartupServices = new System.Windows.Forms.CheckBox();
            this.chbStartupNotifications = new System.Windows.Forms.CheckBox();
            this.grbDisplay = new System.Windows.Forms.GroupBox();
            this.flpDisplay = new System.Windows.Forms.FlowLayoutPanel();
            this.chbShowNotificationPopups = new System.Windows.Forms.CheckBox();
            this.chbAlwaysShowAccessIcons = new System.Windows.Forms.CheckBox();
            this.chbShowAllServices = new System.Windows.Forms.CheckBox();
            this.chbShowNewsBanners = new System.Windows.Forms.CheckBox();
            this.chbStartupTopMost = new System.Windows.Forms.CheckBox();
            this.grbAdvanced = new System.Windows.Forms.GroupBox();
            this.flpAdvanced = new System.Windows.Forms.FlowLayoutPanel();
            this.chbChechForUpdates = new System.Windows.Forms.CheckBox();
            this.chbAutomaticUpdateCheck = new System.Windows.Forms.CheckBox();
            this.chbAutomaticUpdate = new System.Windows.Forms.CheckBox();
            this.flpLanguage = new System.Windows.Forms.FlowLayoutPanel();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.cmbSelectedLanguage = new System.Windows.Forms.ComboBox();
            this.tlpChangeSettings = new System.Windows.Forms.TableLayoutPanel();
            this.btnDefaultSettings = new System.Windows.Forms.Button();
            this.btnCancleSettings = new System.Windows.Forms.Button();
            this.btnOKSettings = new System.Windows.Forms.Button();
            this.tabPageAboutUS = new System.Windows.Forms.TabPage();
            this.tlpAboutUs = new System.Windows.Forms.TableLayoutPanel();
            this.tlpANP = new System.Windows.Forms.TableLayoutPanel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProgrammerName = new System.Windows.Forms.Label();
            this.linkLblEmail = new System.Windows.Forms.LinkLabel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.imageListControlMain25 = new System.Windows.Forms.ImageList(this.components);
            this.toolStripLogo.SuspendLayout();
            this.toolStripMenu.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageMain.SuspendLayout();
            this.tabPageTools.SuspendLayout();
            this.tlpTools.SuspendLayout();
            this.tlpUpTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerConnectify)).BeginInit();
            this.splitContainerConnectify.Panel1.SuspendLayout();
            this.splitContainerConnectify.Panel2.SuspendLayout();
            this.splitContainerConnectify.SuspendLayout();
            this.tlpVisualConnectify.SuspendLayout();
            this.tlpCnfHotspotName.SuspendLayout();
            this.tlpCnfHotspotPassword.SuspendLayout();
            this.panelCMDCommands.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.tlpSettings.SuspendLayout();
            this.grbStartup.SuspendLayout();
            this.flpStartup.SuspendLayout();
            this.grbDisplay.SuspendLayout();
            this.flpDisplay.SuspendLayout();
            this.grbAdvanced.SuspendLayout();
            this.flpAdvanced.SuspendLayout();
            this.flpLanguage.SuspendLayout();
            this.tlpChangeSettings.SuspendLayout();
            this.tabPageAboutUS.SuspendLayout();
            this.tlpAboutUs.SuspendLayout();
            this.tlpANP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Turquoise;
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblMainProductName});
            this.toolStripLogo.Location = new System.Drawing.Point(0, 25);
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripLogo.Size = new System.Drawing.Size(364, 53);
            this.toolStripLogo.TabIndex = 7;
            this.toolStripLogo.Text = "toolStrip2";
            this.toolStripLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.toolStripLogo.MouseHover += new System.EventHandler(this.toolStripLogo_MouseHover);
            this.toolStripLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblProductLogo
            // 
            this.lblProductLogo.AutoSize = false;
            this.lblProductLogo.BackgroundImage = global::AdsServicesApp_hIVE.Properties.Resources.lblLogo;
            this.lblProductLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProductLogo.ForeColor = System.Drawing.Color.Red;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.Size = new System.Drawing.Size(50, 50);
            this.lblProductLogo.Text = "لوگو";
            this.lblProductLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblProductLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblMainProductName
            // 
            this.lblMainProductName.Font = new System.Drawing.Font("B Jadid", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblMainProductName.ForeColor = System.Drawing.Color.Red;
            this.lblMainProductName.Name = "lblMainProductName";
            this.lblMainProductName.Size = new System.Drawing.Size(231, 50);
            this.lblMainProductName.Text = "برنامه خدمات تبلیغاتی - کندو";
            this.lblMainProductName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.lblMainProductName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseMove);
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.LightSeaGreen;
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSettings,
            this.btnTools,
            this.btnAboutUS});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripMenu.Size = new System.Drawing.Size(364, 25);
            this.toolStripMenu.TabIndex = 6;
            this.toolStripMenu.Text = "toolStripMenu";
            // 
            // btnSettings
            // 
            this.btnSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(56, 22);
            this.btnSettings.Text = "تنضیمات";
            this.btnSettings.Visible = false;
            // 
            // btnTools
            // 
            this.btnTools.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnTools.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTools.Name = "btnTools";
            this.btnTools.Size = new System.Drawing.Size(40, 22);
            this.btnTools.Text = "ابزارها";
            this.btnTools.Visible = false;
            // 
            // btnAboutUS
            // 
            this.btnAboutUS.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnAboutUS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAboutUS.Name = "btnAboutUS";
            this.btnAboutUS.Size = new System.Drawing.Size(49, 22);
            this.btnAboutUS.Text = "درباره‌ما";
            this.btnAboutUS.Visible = false;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControlMain.Controls.Add(this.tabPageMain);
            this.tabControlMain.Controls.Add(this.tabPageTools);
            this.tabControlMain.Controls.Add(this.tabPageSettings);
            this.tabControlMain.Controls.Add(this.tabPageAboutUS);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.ImageList = this.imageListControlMain25;
            this.tabControlMain.ItemSize = new System.Drawing.Size(80, 40);
            this.tabControlMain.Location = new System.Drawing.Point(0, 78);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControlMain.RightToLeftLayout = true;
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(364, 563);
            this.tabControlMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlMain.TabIndex = 8;
            this.tabControlMain.Tag = "";
            this.tabControlMain.Enter += new System.EventHandler(this.tabControlMain_Enter);
            // 
            // tabPageMain
            // 
            this.tabPageMain.BackColor = System.Drawing.Color.Honeydew;
            this.tabPageMain.Controls.Add(this.listViewAdsShow);
            this.tabPageMain.ImageIndex = 0;
            this.tabPageMain.Location = new System.Drawing.Point(4, 44);
            this.tabPageMain.Name = "tabPageMain";
            this.tabPageMain.Padding = new System.Windows.Forms.Padding(10);
            this.tabPageMain.Size = new System.Drawing.Size(356, 515);
            this.tabPageMain.TabIndex = 0;
            this.tabPageMain.Text = "خانه";
            // 
            // listViewAdsShow
            // 
            this.listViewAdsShow.BackColor = System.Drawing.Color.White;
            this.listViewAdsShow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewAdsShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewAdsShow.LargeImageList = this.imgListTableAds256x144;
            this.listViewAdsShow.Location = new System.Drawing.Point(10, 10);
            this.listViewAdsShow.MultiSelect = false;
            this.listViewAdsShow.Name = "listViewAdsShow";
            this.listViewAdsShow.Size = new System.Drawing.Size(336, 495);
            this.listViewAdsShow.TabIndex = 0;
            this.listViewAdsShow.UseCompatibleStateImageBehavior = false;
            this.listViewAdsShow.SelectedIndexChanged += new System.EventHandler(this.listViewAdsShow_SelectedIndexChanged);
            // 
            // imgListTableAds256x144
            // 
            this.imgListTableAds256x144.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imgListTableAds256x144.ImageSize = new System.Drawing.Size(256, 144);
            this.imgListTableAds256x144.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tabPageTools
            // 
            this.tabPageTools.BackColor = System.Drawing.Color.White;
            this.tabPageTools.Controls.Add(this.tlpTools);
            this.tabPageTools.ImageIndex = 2;
            this.tabPageTools.Location = new System.Drawing.Point(4, 44);
            this.tabPageTools.Name = "tabPageTools";
            this.tabPageTools.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTools.Size = new System.Drawing.Size(356, 515);
            this.tabPageTools.TabIndex = 2;
            this.tabPageTools.Text = "ابزارها";
            // 
            // tlpTools
            // 
            this.tlpTools.BackColor = System.Drawing.Color.Transparent;
            this.tlpTools.ColumnCount = 1;
            this.tlpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpTools.Controls.Add(this.tlpUpTools, 0, 0);
            this.tlpTools.Controls.Add(this.splitContainerConnectify, 0, 1);
            this.tlpTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpTools.Location = new System.Drawing.Point(3, 3);
            this.tlpTools.Name = "tlpTools";
            this.tlpTools.RowCount = 2;
            this.tlpTools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpTools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlpTools.Size = new System.Drawing.Size(350, 509);
            this.tlpTools.TabIndex = 0;
            // 
            // tlpUpTools
            // 
            this.tlpUpTools.ColumnCount = 3;
            this.tlpUpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tlpUpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
            this.tlpUpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tlpUpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpUpTools.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpUpTools.Controls.Add(this.btnNotepad, 0, 0);
            this.tlpUpTools.Controls.Add(this.btnCalculator, 1, 0);
            this.tlpUpTools.Controls.Add(this.btnPaint, 2, 0);
            this.tlpUpTools.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpUpTools.Location = new System.Drawing.Point(10, 10);
            this.tlpUpTools.Margin = new System.Windows.Forms.Padding(10);
            this.tlpUpTools.Name = "tlpUpTools";
            this.tlpUpTools.RowCount = 1;
            this.tlpUpTools.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpUpTools.Size = new System.Drawing.Size(330, 81);
            this.tlpUpTools.TabIndex = 0;
            // 
            // btnNotepad
            // 
            this.btnNotepad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNotepad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotepad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnNotepad.ImageIndex = 0;
            this.btnNotepad.ImageList = this.imageListButton56;
            this.btnNotepad.Location = new System.Drawing.Point(230, 5);
            this.btnNotepad.Margin = new System.Windows.Forms.Padding(5);
            this.btnNotepad.Name = "btnNotepad";
            this.btnNotepad.Padding = new System.Windows.Forms.Padding(3);
            this.btnNotepad.Size = new System.Drawing.Size(95, 71);
            this.btnNotepad.TabIndex = 0;
            this.btnNotepad.Text = "Notepad";
            this.btnNotepad.UseVisualStyleBackColor = true;
            this.btnNotepad.Click += new System.EventHandler(this.btnNotepad_Click);
            // 
            // imageListButton56
            // 
            this.imageListButton56.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton56.ImageStream")));
            this.imageListButton56.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton56.Images.SetKeyName(0, "btnNotepad.png");
            this.imageListButton56.Images.SetKeyName(1, "btnCalculator.png");
            this.imageListButton56.Images.SetKeyName(2, "btnPaint.png");
            // 
            // btnCalculator
            // 
            this.btnCalculator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalculator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCalculator.ImageIndex = 1;
            this.btnCalculator.ImageList = this.imageListButton56;
            this.btnCalculator.Location = new System.Drawing.Point(112, 5);
            this.btnCalculator.Margin = new System.Windows.Forms.Padding(5);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.Padding = new System.Windows.Forms.Padding(3);
            this.btnCalculator.Size = new System.Drawing.Size(108, 71);
            this.btnCalculator.TabIndex = 1;
            this.btnCalculator.Text = "ماشین حساب";
            this.btnCalculator.UseVisualStyleBackColor = true;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // btnPaint
            // 
            this.btnPaint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPaint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPaint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnPaint.ImageIndex = 2;
            this.btnPaint.ImageList = this.imageListButton56;
            this.btnPaint.Location = new System.Drawing.Point(5, 5);
            this.btnPaint.Margin = new System.Windows.Forms.Padding(5);
            this.btnPaint.Name = "btnPaint";
            this.btnPaint.Padding = new System.Windows.Forms.Padding(3);
            this.btnPaint.Size = new System.Drawing.Size(97, 71);
            this.btnPaint.TabIndex = 2;
            this.btnPaint.Text = "Paint";
            this.btnPaint.UseVisualStyleBackColor = true;
            this.btnPaint.Click += new System.EventHandler(this.btnPaint_Click);
            // 
            // splitContainerConnectify
            // 
            this.splitContainerConnectify.BackColor = System.Drawing.Color.Transparent;
            this.splitContainerConnectify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerConnectify.Location = new System.Drawing.Point(3, 104);
            this.splitContainerConnectify.Name = "splitContainerConnectify";
            this.splitContainerConnectify.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerConnectify.Panel1
            // 
            this.splitContainerConnectify.Panel1.Controls.Add(this.tlpVisualConnectify);
            this.splitContainerConnectify.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // splitContainerConnectify.Panel2
            // 
            this.splitContainerConnectify.Panel2.BackColor = System.Drawing.Color.DodgerBlue;
            this.splitContainerConnectify.Panel2.Controls.Add(this.panelCMDCommands);
            this.splitContainerConnectify.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.splitContainerConnectify.Size = new System.Drawing.Size(344, 402);
            this.splitContainerConnectify.SplitterDistance = 255;
            this.splitContainerConnectify.TabIndex = 1;
            // 
            // tlpVisualConnectify
            // 
            this.tlpVisualConnectify.ColumnCount = 1;
            this.tlpVisualConnectify.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVisualConnectify.Controls.Add(this.tlpCnfHotspotName, 0, 2);
            this.tlpVisualConnectify.Controls.Add(this.lblCnfDetailsProcess, 0, 0);
            this.tlpVisualConnectify.Controls.Add(this.lblCnfHotspotName, 0, 1);
            this.tlpVisualConnectify.Controls.Add(this.lblCnfPassword, 0, 3);
            this.tlpVisualConnectify.Controls.Add(this.btnStartStopConnectify, 0, 5);
            this.tlpVisualConnectify.Controls.Add(this.tlpCnfHotspotPassword, 0, 4);
            this.tlpVisualConnectify.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpVisualConnectify.Location = new System.Drawing.Point(0, 0);
            this.tlpVisualConnectify.Name = "tlpVisualConnectify";
            this.tlpVisualConnectify.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tlpVisualConnectify.RowCount = 6;
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpVisualConnectify.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpVisualConnectify.Size = new System.Drawing.Size(344, 255);
            this.tlpVisualConnectify.TabIndex = 0;
            // 
            // tlpCnfHotspotName
            // 
            this.tlpCnfHotspotName.ColumnCount = 2;
            this.tlpCnfHotspotName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCnfHotspotName.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tlpCnfHotspotName.Controls.Add(this.txtCnfHotspotNameWebsite, 0, 0);
            this.tlpCnfHotspotName.Controls.Add(this.txtCnfHotspotName, 0, 0);
            this.tlpCnfHotspotName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCnfHotspotName.Location = new System.Drawing.Point(20, 91);
            this.tlpCnfHotspotName.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.tlpCnfHotspotName.Name = "tlpCnfHotspotName";
            this.tlpCnfHotspotName.RowCount = 1;
            this.tlpCnfHotspotName.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCnfHotspotName.Size = new System.Drawing.Size(304, 32);
            this.tlpCnfHotspotName.TabIndex = 8;
            // 
            // txtCnfHotspotNameWebsite
            // 
            this.txtCnfHotspotNameWebsite.BackColor = System.Drawing.SystemColors.Window;
            this.txtCnfHotspotNameWebsite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCnfHotspotNameWebsite.ForeColor = System.Drawing.Color.Blue;
            this.txtCnfHotspotNameWebsite.Location = new System.Drawing.Point(232, 3);
            this.txtCnfHotspotNameWebsite.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCnfHotspotNameWebsite.Name = "txtCnfHotspotNameWebsite";
            this.txtCnfHotspotNameWebsite.ReadOnly = true;
            this.txtCnfHotspotNameWebsite.Size = new System.Drawing.Size(72, 20);
            this.txtCnfHotspotNameWebsite.TabIndex = 9;
            this.txtCnfHotspotNameWebsite.Text = "_hIVE72.iR";
            // 
            // txtCnfHotspotName
            // 
            this.txtCnfHotspotName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCnfHotspotName.Location = new System.Drawing.Point(0, 3);
            this.txtCnfHotspotName.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCnfHotspotName.Name = "txtCnfHotspotName";
            this.txtCnfHotspotName.Size = new System.Drawing.Size(232, 20);
            this.txtCnfHotspotName.TabIndex = 8;
            this.txtCnfHotspotName.TextChanged += new System.EventHandler(this.txtCnfHotspotName_TextChanged);
            // 
            // lblCnfDetailsProcess
            // 
            this.lblCnfDetailsProcess.AutoSize = true;
            this.lblCnfDetailsProcess.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCnfDetailsProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblCnfDetailsProcess.Location = new System.Drawing.Point(20, 10);
            this.lblCnfDetailsProcess.Margin = new System.Windows.Forms.Padding(20, 10, 20, 20);
            this.lblCnfDetailsProcess.Name = "lblCnfDetailsProcess";
            this.lblCnfDetailsProcess.Size = new System.Drawing.Size(152, 20);
            this.lblCnfDetailsProcess.TabIndex = 1;
            this.lblCnfDetailsProcess.Text = "Create a Wi-Fi Hotspot ...";
            // 
            // lblCnfHotspotName
            // 
            this.lblCnfHotspotName.AutoSize = true;
            this.lblCnfHotspotName.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCnfHotspotName.Location = new System.Drawing.Point(20, 65);
            this.lblCnfHotspotName.Margin = new System.Windows.Forms.Padding(20, 15, 20, 3);
            this.lblCnfHotspotName.Name = "lblCnfHotspotName";
            this.lblCnfHotspotName.Size = new System.Drawing.Size(134, 20);
            this.lblCnfHotspotName.TabIndex = 2;
            this.lblCnfHotspotName.Text = "Hotspot Name ( نام شبکه  )";
            // 
            // lblCnfPassword
            // 
            this.lblCnfPassword.AutoSize = true;
            this.lblCnfPassword.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCnfPassword.Location = new System.Drawing.Point(20, 141);
            this.lblCnfPassword.Margin = new System.Windows.Forms.Padding(20, 15, 20, 3);
            this.lblCnfPassword.Name = "lblCnfPassword";
            this.lblCnfPassword.Size = new System.Drawing.Size(113, 20);
            this.lblCnfPassword.TabIndex = 4;
            this.lblCnfPassword.Text = "Password ( کلمه عبور )";
            // 
            // btnStartStopConnectify
            // 
            this.btnStartStopConnectify.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartStopConnectify.Location = new System.Drawing.Point(50, 207);
            this.btnStartStopConnectify.Margin = new System.Windows.Forms.Padding(50, 5, 50, 3);
            this.btnStartStopConnectify.Name = "btnStartStopConnectify";
            this.btnStartStopConnectify.Size = new System.Drawing.Size(244, 36);
            this.btnStartStopConnectify.TabIndex = 6;
            this.btnStartStopConnectify.Text = "Start Hotspot";
            this.btnStartStopConnectify.UseVisualStyleBackColor = true;
            this.btnStartStopConnectify.Click += new System.EventHandler(this.btnStartStopConnectify_Click);
            // 
            // tlpCnfHotspotPassword
            // 
            this.tlpCnfHotspotPassword.ColumnCount = 2;
            this.tlpCnfHotspotPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCnfHotspotPassword.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tlpCnfHotspotPassword.Controls.Add(this.txtCnfHotspotPassword, 0, 0);
            this.tlpCnfHotspotPassword.Controls.Add(this.btnShowHidePassword, 1, 0);
            this.tlpCnfHotspotPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpCnfHotspotPassword.Location = new System.Drawing.Point(20, 167);
            this.tlpCnfHotspotPassword.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.tlpCnfHotspotPassword.Name = "tlpCnfHotspotPassword";
            this.tlpCnfHotspotPassword.RowCount = 1;
            this.tlpCnfHotspotPassword.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpCnfHotspotPassword.Size = new System.Drawing.Size(304, 32);
            this.tlpCnfHotspotPassword.TabIndex = 7;
            // 
            // txtCnfHotspotPassword
            // 
            this.txtCnfHotspotPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCnfHotspotPassword.Location = new System.Drawing.Point(0, 3);
            this.txtCnfHotspotPassword.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.txtCnfHotspotPassword.Name = "txtCnfHotspotPassword";
            this.txtCnfHotspotPassword.Size = new System.Drawing.Size(232, 20);
            this.txtCnfHotspotPassword.TabIndex = 6;
            this.txtCnfHotspotPassword.TextChanged += new System.EventHandler(this.txtCnfHotspotPassword_TextChanged);
            this.txtCnfHotspotPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCnfHotspotPassword_KeyPress);
            // 
            // btnShowHidePassword
            // 
            this.btnShowHidePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowHidePassword.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnShowHidePassword.Location = new System.Drawing.Point(242, 1);
            this.btnShowHidePassword.Margin = new System.Windows.Forms.Padding(10, 1, 3, 3);
            this.btnShowHidePassword.Name = "btnShowHidePassword";
            this.btnShowHidePassword.Size = new System.Drawing.Size(59, 23);
            this.btnShowHidePassword.TabIndex = 7;
            this.btnShowHidePassword.Text = "نمایش";
            this.btnShowHidePassword.UseVisualStyleBackColor = true;
            this.btnShowHidePassword.Click += new System.EventHandler(this.btnShowHidePassword_Click);
            // 
            // panelCMDCommands
            // 
            this.panelCMDCommands.Controls.Add(this.btnClearCMDCommands);
            this.panelCMDCommands.Controls.Add(this.rtbCMDCommands);
            this.panelCMDCommands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCMDCommands.Location = new System.Drawing.Point(0, 0);
            this.panelCMDCommands.Name = "panelCMDCommands";
            this.panelCMDCommands.Padding = new System.Windows.Forms.Padding(10);
            this.panelCMDCommands.Size = new System.Drawing.Size(344, 143);
            this.panelCMDCommands.TabIndex = 0;
            // 
            // btnClearCMDCommands
            // 
            this.btnClearCMDCommands.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearCMDCommands.BackColor = System.Drawing.Color.SkyBlue;
            this.btnClearCMDCommands.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClearCMDCommands.Location = new System.Drawing.Point(275, 110);
            this.btnClearCMDCommands.Margin = new System.Windows.Forms.Padding(10, 1, 3, 3);
            this.btnClearCMDCommands.Name = "btnClearCMDCommands";
            this.btnClearCMDCommands.Size = new System.Drawing.Size(59, 23);
            this.btnClearCMDCommands.TabIndex = 10;
            this.btnClearCMDCommands.Text = "پاک کردن";
            this.btnClearCMDCommands.UseVisualStyleBackColor = false;
            this.btnClearCMDCommands.Click += new System.EventHandler(this.btnClearCMDCommands_Click);
            // 
            // rtbCMDCommands
            // 
            this.rtbCMDCommands.BackColor = System.Drawing.Color.DodgerBlue;
            this.rtbCMDCommands.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbCMDCommands.Location = new System.Drawing.Point(10, 10);
            this.rtbCMDCommands.Name = "rtbCMDCommands";
            this.rtbCMDCommands.ReadOnly = true;
            this.rtbCMDCommands.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rtbCMDCommands.Size = new System.Drawing.Size(324, 123);
            this.rtbCMDCommands.TabIndex = 0;
            this.rtbCMDCommands.Text = "";
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.BackColor = System.Drawing.Color.White;
            this.tabPageSettings.Controls.Add(this.tlpSettings);
            this.tabPageSettings.ImageIndex = 1;
            this.tabPageSettings.Location = new System.Drawing.Point(4, 44);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(356, 515);
            this.tabPageSettings.TabIndex = 1;
            this.tabPageSettings.Text = "تنظیمات";
            // 
            // tlpSettings
            // 
            this.tlpSettings.ColumnCount = 1;
            this.tlpSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpSettings.Controls.Add(this.grbStartup, 0, 0);
            this.tlpSettings.Controls.Add(this.grbDisplay, 0, 1);
            this.tlpSettings.Controls.Add(this.grbAdvanced, 0, 2);
            this.tlpSettings.Controls.Add(this.tlpChangeSettings, 0, 3);
            this.tlpSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpSettings.Location = new System.Drawing.Point(3, 3);
            this.tlpSettings.Name = "tlpSettings";
            this.tlpSettings.Padding = new System.Windows.Forms.Padding(15);
            this.tlpSettings.RowCount = 4;
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpSettings.Size = new System.Drawing.Size(350, 509);
            this.tlpSettings.TabIndex = 0;
            // 
            // grbStartup
            // 
            this.grbStartup.Controls.Add(this.flpStartup);
            this.grbStartup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbStartup.Location = new System.Drawing.Point(18, 18);
            this.grbStartup.Name = "grbStartup";
            this.grbStartup.Size = new System.Drawing.Size(314, 108);
            this.grbStartup.TabIndex = 1;
            this.grbStartup.TabStop = false;
            this.grbStartup.Text = "راه اندازی ویندوز";
            // 
            // flpStartup
            // 
            this.flpStartup.Controls.Add(this.chbStartupApp);
            this.flpStartup.Controls.Add(this.chbStartupServices);
            this.flpStartup.Controls.Add(this.chbStartupNotifications);
            this.flpStartup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpStartup.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpStartup.Location = new System.Drawing.Point(3, 16);
            this.flpStartup.Name = "flpStartup";
            this.flpStartup.Padding = new System.Windows.Forms.Padding(5);
            this.flpStartup.Size = new System.Drawing.Size(308, 89);
            this.flpStartup.TabIndex = 0;
            // 
            // chbStartupApp
            // 
            this.chbStartupApp.AutoSize = true;
            this.chbStartupApp.Location = new System.Drawing.Point(211, 8);
            this.chbStartupApp.Name = "chbStartupApp";
            this.chbStartupApp.Size = new System.Drawing.Size(84, 17);
            this.chbStartupApp.TabIndex = 0;
            this.chbStartupApp.Text = "اجرای برنامه";
            this.chbStartupApp.UseVisualStyleBackColor = true;
            this.chbStartupApp.CheckedChanged += new System.EventHandler(this.chbStartupApp_CheckedChanged);
            // 
            // chbStartupServices
            // 
            this.chbStartupServices.AutoSize = true;
            this.chbStartupServices.Location = new System.Drawing.Point(209, 31);
            this.chbStartupServices.Name = "chbStartupServices";
            this.chbStartupServices.Size = new System.Drawing.Size(86, 17);
            this.chbStartupServices.TabIndex = 1;
            this.chbStartupServices.Text = "اجرای خدمات";
            this.chbStartupServices.UseVisualStyleBackColor = true;
            this.chbStartupServices.CheckedChanged += new System.EventHandler(this.chbStartupServices_CheckedChanged);
            // 
            // chbStartupNotifications
            // 
            this.chbStartupNotifications.AutoSize = true;
            this.chbStartupNotifications.Location = new System.Drawing.Point(199, 54);
            this.chbStartupNotifications.Name = "chbStartupNotifications";
            this.chbStartupNotifications.Size = new System.Drawing.Size(96, 17);
            this.chbStartupNotifications.TabIndex = 2;
            this.chbStartupNotifications.Text = "اجرای آگاه‌ساز";
            this.chbStartupNotifications.UseVisualStyleBackColor = true;
            this.chbStartupNotifications.CheckedChanged += new System.EventHandler(this.chbStartupNotifications_CheckedChanged);
            // 
            // grbDisplay
            // 
            this.grbDisplay.Controls.Add(this.flpDisplay);
            this.grbDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbDisplay.Location = new System.Drawing.Point(18, 132);
            this.grbDisplay.Name = "grbDisplay";
            this.grbDisplay.Size = new System.Drawing.Size(314, 156);
            this.grbDisplay.TabIndex = 2;
            this.grbDisplay.TabStop = false;
            this.grbDisplay.Text = "آشکارساز";
            // 
            // flpDisplay
            // 
            this.flpDisplay.Controls.Add(this.chbShowNotificationPopups);
            this.flpDisplay.Controls.Add(this.chbAlwaysShowAccessIcons);
            this.flpDisplay.Controls.Add(this.chbShowAllServices);
            this.flpDisplay.Controls.Add(this.chbShowNewsBanners);
            this.flpDisplay.Controls.Add(this.chbStartupTopMost);
            this.flpDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDisplay.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpDisplay.Location = new System.Drawing.Point(3, 16);
            this.flpDisplay.Name = "flpDisplay";
            this.flpDisplay.Padding = new System.Windows.Forms.Padding(5);
            this.flpDisplay.Size = new System.Drawing.Size(308, 137);
            this.flpDisplay.TabIndex = 1;
            // 
            // chbShowNotificationPopups
            // 
            this.chbShowNotificationPopups.AutoSize = true;
            this.chbShowNotificationPopups.Location = new System.Drawing.Point(148, 8);
            this.chbShowNotificationPopups.Name = "chbShowNotificationPopups";
            this.chbShowNotificationPopups.Size = new System.Drawing.Size(147, 17);
            this.chbShowNotificationPopups.TabIndex = 0;
            this.chbShowNotificationPopups.Text = "نمایش پرهای اطلاع رسانی";
            this.chbShowNotificationPopups.UseVisualStyleBackColor = true;
            this.chbShowNotificationPopups.CheckedChanged += new System.EventHandler(this.chbShowNotificationPopups_CheckedChanged);
            // 
            // chbAlwaysShowAccessIcons
            // 
            this.chbAlwaysShowAccessIcons.AutoSize = true;
            this.chbAlwaysShowAccessIcons.Location = new System.Drawing.Point(110, 31);
            this.chbAlwaysShowAccessIcons.Name = "chbAlwaysShowAccessIcons";
            this.chbAlwaysShowAccessIcons.Size = new System.Drawing.Size(185, 17);
            this.chbAlwaysShowAccessIcons.TabIndex = 1;
            this.chbAlwaysShowAccessIcons.Text = "نمایش همیشه آیکون های دسترسی";
            this.chbAlwaysShowAccessIcons.UseVisualStyleBackColor = true;
            this.chbAlwaysShowAccessIcons.CheckedChanged += new System.EventHandler(this.chbAlwaysShowAccessIcons_CheckedChanged);
            // 
            // chbShowAllServices
            // 
            this.chbShowAllServices.AutoSize = true;
            this.chbShowAllServices.Location = new System.Drawing.Point(191, 54);
            this.chbShowAllServices.Name = "chbShowAllServices";
            this.chbShowAllServices.Size = new System.Drawing.Size(104, 17);
            this.chbShowAllServices.TabIndex = 2;
            this.chbShowAllServices.Text = "نمایش همه خدمات";
            this.chbShowAllServices.UseVisualStyleBackColor = true;
            this.chbShowAllServices.CheckedChanged += new System.EventHandler(this.chbShowAllServices_CheckedChanged);
            // 
            // chbShowNewsBanners
            // 
            this.chbShowNewsBanners.AutoSize = true;
            this.chbShowNewsBanners.Location = new System.Drawing.Point(144, 77);
            this.chbShowNewsBanners.Name = "chbShowNewsBanners";
            this.chbShowNewsBanners.Size = new System.Drawing.Size(151, 17);
            this.chbShowNewsBanners.TabIndex = 3;
            this.chbShowNewsBanners.Text = "نشان دادن اخبار / آگهی ها";
            this.chbShowNewsBanners.UseVisualStyleBackColor = true;
            this.chbShowNewsBanners.CheckedChanged += new System.EventHandler(this.chbShowNewsBanners_CheckedChanged);
            // 
            // chbStartupTopMost
            // 
            this.chbStartupTopMost.AutoSize = true;
            this.chbStartupTopMost.Location = new System.Drawing.Point(85, 100);
            this.chbStartupTopMost.Name = "chbStartupTopMost";
            this.chbStartupTopMost.Size = new System.Drawing.Size(210, 17);
            this.chbStartupTopMost.TabIndex = 4;
            this.chbStartupTopMost.Text = "سنجاق کردن ( نمایش روی همه پنجره‌ها )";
            this.chbStartupTopMost.UseVisualStyleBackColor = true;
            this.chbStartupTopMost.CheckedChanged += new System.EventHandler(this.chbStartupTopMost_CheckedChanged);
            // 
            // grbAdvanced
            // 
            this.grbAdvanced.Controls.Add(this.flpAdvanced);
            this.grbAdvanced.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbAdvanced.Location = new System.Drawing.Point(18, 294);
            this.grbAdvanced.Name = "grbAdvanced";
            this.grbAdvanced.Size = new System.Drawing.Size(314, 137);
            this.grbAdvanced.TabIndex = 3;
            this.grbAdvanced.TabStop = false;
            this.grbAdvanced.Text = "پیشرفته";
            // 
            // flpAdvanced
            // 
            this.flpAdvanced.Controls.Add(this.chbChechForUpdates);
            this.flpAdvanced.Controls.Add(this.chbAutomaticUpdateCheck);
            this.flpAdvanced.Controls.Add(this.chbAutomaticUpdate);
            this.flpAdvanced.Controls.Add(this.flpLanguage);
            this.flpAdvanced.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAdvanced.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpAdvanced.Location = new System.Drawing.Point(3, 16);
            this.flpAdvanced.Name = "flpAdvanced";
            this.flpAdvanced.Padding = new System.Windows.Forms.Padding(5);
            this.flpAdvanced.Size = new System.Drawing.Size(308, 118);
            this.flpAdvanced.TabIndex = 2;
            // 
            // chbChechForUpdates
            // 
            this.chbChechForUpdates.AutoSize = true;
            this.chbChechForUpdates.Location = new System.Drawing.Point(125, 8);
            this.chbChechForUpdates.Name = "chbChechForUpdates";
            this.chbChechForUpdates.Size = new System.Drawing.Size(170, 17);
            this.chbChechForUpdates.TabIndex = 1;
            this.chbChechForUpdates.Text = "به روز رسانی ها رو بررسی کن";
            this.chbChechForUpdates.UseVisualStyleBackColor = true;
            this.chbChechForUpdates.CheckedChanged += new System.EventHandler(this.chbChechForUpdates_CheckedChanged);
            // 
            // chbAutomaticUpdateCheck
            // 
            this.chbAutomaticUpdateCheck.AutoSize = true;
            this.chbAutomaticUpdateCheck.Location = new System.Drawing.Point(133, 31);
            this.chbAutomaticUpdateCheck.Name = "chbAutomaticUpdateCheck";
            this.chbAutomaticUpdateCheck.Size = new System.Drawing.Size(162, 17);
            this.chbAutomaticUpdateCheck.TabIndex = 2;
            this.chbAutomaticUpdateCheck.Text = "بررسی به روز رسانی خودکار";
            this.chbAutomaticUpdateCheck.UseVisualStyleBackColor = true;
            this.chbAutomaticUpdateCheck.CheckedChanged += new System.EventHandler(this.chbAutomaticUpdateCheck_CheckedChanged);
            // 
            // chbAutomaticUpdate
            // 
            this.chbAutomaticUpdate.AutoSize = true;
            this.chbAutomaticUpdate.Location = new System.Drawing.Point(169, 54);
            this.chbAutomaticUpdate.Name = "chbAutomaticUpdate";
            this.chbAutomaticUpdate.Size = new System.Drawing.Size(126, 17);
            this.chbAutomaticUpdate.TabIndex = 3;
            this.chbAutomaticUpdate.Text = "به روز رسانی خودکار";
            this.chbAutomaticUpdate.UseVisualStyleBackColor = true;
            this.chbAutomaticUpdate.CheckedChanged += new System.EventHandler(this.chbAutomaticUpdate_CheckedChanged);
            // 
            // flpLanguage
            // 
            this.flpLanguage.Controls.Add(this.lblLanguage);
            this.flpLanguage.Controls.Add(this.cmbSelectedLanguage);
            this.flpLanguage.Location = new System.Drawing.Point(3, 77);
            this.flpLanguage.Name = "flpLanguage";
            this.flpLanguage.Size = new System.Drawing.Size(292, 30);
            this.flpLanguage.TabIndex = 4;
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(232, 6);
            this.lblLanguage.Margin = new System.Windows.Forms.Padding(0, 6, 3, 3);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(60, 13);
            this.lblLanguage.TabIndex = 0;
            this.lblLanguage.Text = "زبان برنامه:";
            // 
            // cmbSelectedLanguage
            // 
            this.cmbSelectedLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSelectedLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmbSelectedLanguage.FormattingEnabled = true;
            this.cmbSelectedLanguage.Items.AddRange(new object[] {
            "English",
            "فارسی"});
            this.cmbSelectedLanguage.Location = new System.Drawing.Point(86, 3);
            this.cmbSelectedLanguage.Name = "cmbSelectedLanguage";
            this.cmbSelectedLanguage.Size = new System.Drawing.Size(140, 21);
            this.cmbSelectedLanguage.Sorted = true;
            this.cmbSelectedLanguage.TabIndex = 1;
            this.cmbSelectedLanguage.SelectedIndexChanged += new System.EventHandler(this.cmbSelectedLanguage_SelectedIndexChanged);
            // 
            // tlpChangeSettings
            // 
            this.tlpChangeSettings.ColumnCount = 3;
            this.tlpChangeSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpChangeSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpChangeSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpChangeSettings.Controls.Add(this.btnDefaultSettings, 0, 0);
            this.tlpChangeSettings.Controls.Add(this.btnCancleSettings, 0, 0);
            this.tlpChangeSettings.Controls.Add(this.btnOKSettings, 0, 0);
            this.tlpChangeSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpChangeSettings.Location = new System.Drawing.Point(20, 439);
            this.tlpChangeSettings.Margin = new System.Windows.Forms.Padding(5);
            this.tlpChangeSettings.Name = "tlpChangeSettings";
            this.tlpChangeSettings.RowCount = 1;
            this.tlpChangeSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpChangeSettings.Size = new System.Drawing.Size(310, 50);
            this.tlpChangeSettings.TabIndex = 4;
            // 
            // btnDefaultSettings
            // 
            this.btnDefaultSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDefaultSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDefaultSettings.Location = new System.Drawing.Point(5, 5);
            this.btnDefaultSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnDefaultSettings.Name = "btnDefaultSettings";
            this.btnDefaultSettings.Padding = new System.Windows.Forms.Padding(3);
            this.btnDefaultSettings.Size = new System.Drawing.Size(94, 40);
            this.btnDefaultSettings.TabIndex = 4;
            this.btnDefaultSettings.Text = "پیش‌فرض";
            this.btnDefaultSettings.UseVisualStyleBackColor = true;
            this.btnDefaultSettings.Click += new System.EventHandler(this.btnDefaultSettings_Click);
            // 
            // btnCancleSettings
            // 
            this.btnCancleSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancleSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancleSettings.Location = new System.Drawing.Point(109, 5);
            this.btnCancleSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnCancleSettings.Name = "btnCancleSettings";
            this.btnCancleSettings.Padding = new System.Windows.Forms.Padding(3);
            this.btnCancleSettings.Size = new System.Drawing.Size(93, 40);
            this.btnCancleSettings.TabIndex = 3;
            this.btnCancleSettings.Text = "انصراف";
            this.btnCancleSettings.UseVisualStyleBackColor = true;
            this.btnCancleSettings.Click += new System.EventHandler(this.btnCancleSettings_Click);
            // 
            // btnOKSettings
            // 
            this.btnOKSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOKSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOKSettings.Location = new System.Drawing.Point(212, 5);
            this.btnOKSettings.Margin = new System.Windows.Forms.Padding(5);
            this.btnOKSettings.Name = "btnOKSettings";
            this.btnOKSettings.Padding = new System.Windows.Forms.Padding(3);
            this.btnOKSettings.Size = new System.Drawing.Size(93, 40);
            this.btnOKSettings.TabIndex = 2;
            this.btnOKSettings.Text = "تایید";
            this.btnOKSettings.UseVisualStyleBackColor = true;
            this.btnOKSettings.Click += new System.EventHandler(this.btnOKSettings_Click);
            // 
            // tabPageAboutUS
            // 
            this.tabPageAboutUS.BackColor = System.Drawing.Color.White;
            this.tabPageAboutUS.Controls.Add(this.tlpAboutUs);
            this.tabPageAboutUS.ImageIndex = 3;
            this.tabPageAboutUS.Location = new System.Drawing.Point(4, 44);
            this.tabPageAboutUS.Name = "tabPageAboutUS";
            this.tabPageAboutUS.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAboutUS.Size = new System.Drawing.Size(356, 515);
            this.tabPageAboutUS.TabIndex = 3;
            this.tabPageAboutUS.Text = "درباره‌ما";
            // 
            // tlpAboutUs
            // 
            this.tlpAboutUs.ColumnCount = 1;
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.Controls.Add(this.tlpANP, 0, 2);
            this.tlpAboutUs.Controls.Add(this.picLogo, 0, 0);
            this.tlpAboutUs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutUs.Location = new System.Drawing.Point(3, 3);
            this.tlpAboutUs.Name = "tlpAboutUs";
            this.tlpAboutUs.Padding = new System.Windows.Forms.Padding(20);
            this.tlpAboutUs.RowCount = 3;
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpAboutUs.Size = new System.Drawing.Size(350, 509);
            this.tlpAboutUs.TabIndex = 1;
            // 
            // tlpANP
            // 
            this.tlpANP.ColumnCount = 1;
            this.tlpANP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.Controls.Add(this.lblVersion, 0, 4);
            this.tlpANP.Controls.Add(this.lblProductName, 0, 0);
            this.tlpANP.Controls.Add(this.lblProgrammerName, 0, 1);
            this.tlpANP.Controls.Add(this.linkLblEmail, 0, 3);
            this.tlpANP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpANP.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpANP.Location = new System.Drawing.Point(23, 280);
            this.tlpANP.Name = "tlpANP";
            this.tlpANP.RowCount = 5;
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpANP.Size = new System.Drawing.Size(304, 206);
            this.tlpANP.TabIndex = 33;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblVersion.Location = new System.Drawing.Point(3, 169);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(298, 25);
            this.lblVersion.TabIndex = 336;
            this.lblVersion.Text = "نسخه : ۱.۰۰";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("B Jadid", 12F, System.Drawing.FontStyle.Bold);
            this.lblProductName.ForeColor = System.Drawing.Color.Red;
            this.lblProductName.Location = new System.Drawing.Point(3, 11);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(298, 23);
            this.lblProductName.TabIndex = 312;
            this.lblProductName.Text = "برنامه خدمات تبلیغاتی - کندو";
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProgrammerName
            // 
            this.lblProgrammerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProgrammerName.AutoSize = true;
            this.lblProgrammerName.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProgrammerName.ForeColor = System.Drawing.Color.Blue;
            this.lblProgrammerName.Location = new System.Drawing.Point(3, 56);
            this.lblProgrammerName.Name = "lblProgrammerName";
            this.lblProgrammerName.Size = new System.Drawing.Size(298, 25);
            this.lblProgrammerName.TabIndex = 313;
            this.lblProgrammerName.Text = "hIVE Productions - Mehdi MzITs";
            this.lblProgrammerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLblEmail
            // 
            this.linkLblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblEmail.AutoSize = true;
            this.linkLblEmail.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.linkLblEmail.LinkColor = System.Drawing.Color.DarkMagenta;
            this.linkLblEmail.Location = new System.Drawing.Point(3, 122);
            this.linkLblEmail.Name = "linkLblEmail";
            this.linkLblEmail.Size = new System.Drawing.Size(298, 25);
            this.linkLblEmail.TabIndex = 335;
            this.linkLblEmail.TabStop = true;
            this.linkLblEmail.Text = "hIVE7222@gmail.com";
            this.linkLblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Transparent;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Image = global::AdsServicesApp_hIVE.Properties.Resources.hIVE_Company_Pro___Logo_T455x256;
            this.picLogo.Location = new System.Drawing.Point(23, 23);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(304, 228);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // imageListControlMain25
            // 
            this.imageListControlMain25.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListControlMain25.ImageStream")));
            this.imageListControlMain25.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListControlMain25.Images.SetKeyName(0, "tabHome.png");
            this.imageListControlMain25.Images.SetKeyName(1, "tabSettings.png");
            this.imageListControlMain25.Images.SetKeyName(2, "tabTools.png");
            this.imageListControlMain25.Images.SetKeyName(3, "tabAboutUS.png");
            this.imageListControlMain25.Images.SetKeyName(4, "btnCalculator.png");
            // 
            // FrmStartup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(364, 641);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1000, 700);
            this.MinimumSize = new System.Drawing.Size(380, 680);
            this.Name = "FrmStartup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "برنامه خدمات تبلیغاتی";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StartupForm_FormClosing);
            this.Load += new System.EventHandler(this.FrmStartup_Load);
            this.SizeChanged += new System.EventHandler(this.FrmStartup_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmDragForm_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseMove);
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageMain.ResumeLayout(false);
            this.tabPageTools.ResumeLayout(false);
            this.tlpTools.ResumeLayout(false);
            this.tlpUpTools.ResumeLayout(false);
            this.splitContainerConnectify.Panel1.ResumeLayout(false);
            this.splitContainerConnectify.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerConnectify)).EndInit();
            this.splitContainerConnectify.ResumeLayout(false);
            this.tlpVisualConnectify.ResumeLayout(false);
            this.tlpVisualConnectify.PerformLayout();
            this.tlpCnfHotspotName.ResumeLayout(false);
            this.tlpCnfHotspotName.PerformLayout();
            this.tlpCnfHotspotPassword.ResumeLayout(false);
            this.tlpCnfHotspotPassword.PerformLayout();
            this.panelCMDCommands.ResumeLayout(false);
            this.tabPageSettings.ResumeLayout(false);
            this.tlpSettings.ResumeLayout(false);
            this.grbStartup.ResumeLayout(false);
            this.flpStartup.ResumeLayout(false);
            this.flpStartup.PerformLayout();
            this.grbDisplay.ResumeLayout(false);
            this.flpDisplay.ResumeLayout(false);
            this.flpDisplay.PerformLayout();
            this.grbAdvanced.ResumeLayout(false);
            this.flpAdvanced.ResumeLayout(false);
            this.flpAdvanced.PerformLayout();
            this.flpLanguage.ResumeLayout(false);
            this.flpLanguage.PerformLayout();
            this.tlpChangeSettings.ResumeLayout(false);
            this.tabPageAboutUS.ResumeLayout(false);
            this.tlpAboutUs.ResumeLayout(false);
            this.tlpANP.ResumeLayout(false);
            this.tlpANP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblMainProductName;
        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnSettings;
        private System.Windows.Forms.ToolStripButton btnTools;
        private System.Windows.Forms.ToolStripButton btnAboutUS;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageMain;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.TabPage tabPageTools;
        private System.Windows.Forms.TabPage tabPageAboutUS;
        private System.Windows.Forms.TableLayoutPanel tlpTools;
        private System.Windows.Forms.TableLayoutPanel tlpUpTools;
        private System.Windows.Forms.Button btnNotepad;
        private System.Windows.Forms.Button btnCalculator;
        private System.Windows.Forms.Button btnPaint;
        private System.Windows.Forms.SplitContainer splitContainerConnectify;
        private System.Windows.Forms.TableLayoutPanel tlpVisualConnectify;
        private System.Windows.Forms.Label lblCnfDetailsProcess;
        private System.Windows.Forms.Label lblCnfHotspotName;
        private System.Windows.Forms.Label lblCnfPassword;
        private System.Windows.Forms.Button btnStartStopConnectify;
        private System.Windows.Forms.Panel panelCMDCommands;
        private System.Windows.Forms.TableLayoutPanel tlpCnfHotspotPassword;
        private System.Windows.Forms.TextBox txtCnfHotspotPassword;
        private System.Windows.Forms.Button btnShowHidePassword;
        private System.Windows.Forms.TableLayoutPanel tlpSettings;
        private System.Windows.Forms.GroupBox grbStartup;
        private System.Windows.Forms.GroupBox grbDisplay;
        private System.Windows.Forms.GroupBox grbAdvanced;
        private System.Windows.Forms.FlowLayoutPanel flpStartup;
        private System.Windows.Forms.FlowLayoutPanel flpAdvanced;
        private System.Windows.Forms.CheckBox chbStartupApp;
        private System.Windows.Forms.CheckBox chbStartupServices;
        private System.Windows.Forms.CheckBox chbStartupNotifications;
        private System.Windows.Forms.FlowLayoutPanel flpDisplay;
        private System.Windows.Forms.CheckBox chbShowNotificationPopups;
        private System.Windows.Forms.CheckBox chbAlwaysShowAccessIcons;
        private System.Windows.Forms.CheckBox chbShowAllServices;
        private System.Windows.Forms.CheckBox chbShowNewsBanners;
        private System.Windows.Forms.CheckBox chbChechForUpdates;
        private System.Windows.Forms.CheckBox chbAutomaticUpdateCheck;
        private System.Windows.Forms.CheckBox chbAutomaticUpdate;
        private System.Windows.Forms.FlowLayoutPanel flpLanguage;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.ComboBox cmbSelectedLanguage;
        private System.Windows.Forms.TableLayoutPanel tlpChangeSettings;
        private System.Windows.Forms.Button btnDefaultSettings;
        private System.Windows.Forms.Button btnCancleSettings;
        private System.Windows.Forms.Button btnOKSettings;
        private System.Windows.Forms.TableLayoutPanel tlpAboutUs;
        private System.Windows.Forms.TableLayoutPanel tlpANP;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Label lblProgrammerName;
        private System.Windows.Forms.LinkLabel linkLblEmail;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.ImageList imgListTableAds256x144;
        private System.Windows.Forms.ListView listViewAdsShow;
        private System.Windows.Forms.ImageList imageListControlMain25;
        private System.Windows.Forms.ImageList imageListButton56;
        private System.Windows.Forms.CheckBox chbStartupTopMost;
        private System.Windows.Forms.RichTextBox rtbCMDCommands;
        private System.Windows.Forms.Button btnClearCMDCommands;
        private System.Windows.Forms.TableLayoutPanel tlpCnfHotspotName;
        private System.Windows.Forms.TextBox txtCnfHotspotName;
        private System.Windows.Forms.TextBox txtCnfHotspotNameWebsite;
    }
}

